package com.example.meetingscheduler

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class ResponseModel : Serializable {

    @SerializedName("start_time")
    @Expose
    var startTime: String? = null
    @SerializedName("end_time")
    @Expose
    var endTime: String? = null
    @SerializedName("description")
    @Expose
    var description: String? = null
    @SerializedName("participants")
    @Expose
    var participants: List<String>? = null

}