package com.example.meetingscheduler

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.main_content.*
import org.json.JSONException
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {
    private lateinit var mRequestQueue: RequestQueue
    val baseUrl = "http://fathomless-shelf-5846.herokuapp.com/api/schedule?date="
    lateinit var calendarObject: Calendar
    lateinit var startTimecalendarObject: Calendar
    lateinit var endTimecalendarObject: Calendar
    lateinit var presentCalendarObject: Calendar
    lateinit var calendarDate: Date
    val dateFormat = SimpleDateFormat("dd-MM-yyyy")
    var isCoinciding = false
    var convertedResponse: ArrayList<ResponseModel>? = null
    var newMeetings: ArrayList<ResponseModel> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mRequestQueue = Volley.newRequestQueue(this);
        var toolbar: Toolbar = findViewById(R.id.toolbar)
        var toolbar_title: TextView = toolbar.findViewById(R.id.toolbar_title)
        setSupportActionBar(toolbar)
        toolbar_title.setText(getCurrentDate())
        callApiForDate(toolbar_title.text.toString().replace("-", "/"), true)
        getSupportActionBar()!!.setDisplayShowTitleEnabled(false);
        toolbar_title.setOnClickListener {
            openCalendar()
        }
        nextLayout.setOnClickListener {
            toolbar_title.setText(getNextDate())
            callApiForDate(toolbar_title.text.toString().replace("-", "/"), false)

        }
        prevLayout.setOnClickListener {
            toolbar_title.setText(getPreviousDate())
        }
        tvScheduleMeeting.setOnClickListener {
            scheduleMeeting()
        }
        replaceFragment()

    }

    private fun scheduleMeeting() {
        openDialgoue()
        //openTimePicker()

    }

    private fun openDialgoue() {

        try {
            val dialogBuilder = AlertDialog.Builder(this)
            var inflator: LayoutInflater = this.layoutInflater
            val currentTime = SimpleDateFormat("HH:mm", Locale.getDefault()).format(Date())

            val dialogView: View = inflator.inflate(R.layout.schedulemeetinglayout, null)
            dialogBuilder.setView(dialogView)
            dialogBuilder.setCancelable(false)
            val tvScheduleMeeting: TextView = dialogView.findViewById(R.id.tvScheduleMeeting)
            val endTime: TextView = dialogView.findViewById(R.id.endTime)
            val formattedDate = dateFormat.format(calendarDate)
            dialogView.findViewById<TextView>(R.id.endDay).text = formattedDate
            dialogView.findViewById<TextView>(R.id.startDay).text = formattedDate
            val beginTime: TextView = dialogView.findViewById(R.id.beginTime)
            beginTime.text = currentTime
            endTime.text = currentTime
            var splittedDefaultHour = currentTime.split(":")
            var dialog: AlertDialog = dialogBuilder.create()
            dialog.setCanceledOnTouchOutside(false)
            dialog.setCancelable(false)
            startTimecalendarObject = Calendar.getInstance()
            startTimecalendarObject.set(Calendar.HOUR_OF_DAY, (splittedDefaultHour[0]).toInt())
            endTimecalendarObject = Calendar.getInstance()
            endTimecalendarObject.set(Calendar.HOUR_OF_DAY, (splittedDefaultHour[1]).toInt())

            tvScheduleMeeting.setOnClickListener {
                dialog.dismiss()
                checkForMeetingAvailaibility()

            }
            beginTime.setOnClickListener {
                openTimePicker(beginTime)
            }
            endTime.setOnClickListener {
                openTimePicker(endTime)
            }
            dialog.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun openTimePicker(textView: TextView) {
        val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
            if (textView.id == R.id.beginTime) {
                startTimecalendarObject.set(Calendar.HOUR_OF_DAY, hour)
                startTimecalendarObject.set(Calendar.MINUTE, minute)
                textView.text = SimpleDateFormat("HH:mm").format(startTimecalendarObject.time)

            } else {
                endTimecalendarObject = Calendar.getInstance()
                endTimecalendarObject.set(Calendar.HOUR_OF_DAY, hour)
                endTimecalendarObject.set(Calendar.MINUTE, minute)
                textView.text = SimpleDateFormat("HH:mm").format(endTimecalendarObject.time)

            }
        }
        TimePickerDialog(
            this,
            timeSetListener,
            calendarObject.get(Calendar.HOUR_OF_DAY),
            calendarObject.get(Calendar.MINUTE),
            true
        ).show()


    }


    private fun checkForMeetingAvailaibility() {
        var model = ResponseModel()
        isCoinciding = false
        newMeetings!!.clear()
        for (responseModel in convertedResponse!!) {
            var startArray = responseModel.startTime!!.split(":")
            var endArray = responseModel.endTime!!.split(":")
            if ((startArray[0].toInt()) == startTimecalendarObject.get(Calendar.HOUR_OF_DAY)) {

                if (startTimecalendarObject.get(Calendar.MINUTE) > startArray[1].toInt()) {
                    if (startTimecalendarObject.get(Calendar.HOUR_OF_DAY) == endArray[0].toInt()) {
                        if (startTimecalendarObject.get(Calendar.MINUTE) > endArray[1].toInt()) {
                            if (!isCoinciding) {
                                model.description = "NEW"
                                model.startTime =
                                    startTimecalendarObject.get(Calendar.HOUR_OF_DAY).toString() + ":" + startTimecalendarObject.get(
                                        Calendar.MINUTE
                                    )
                                model.endTime =
                                    endTimecalendarObject.get(Calendar.HOUR_OF_DAY).toString() + ":" + endTimecalendarObject.get(
                                        Calendar.MINUTE
                                    )
                                //   newMeetings.add(model)
                                //  Toast.makeText(this, "Slot Available", Toast.LENGTH_SHORT).show()
                                break
                            }

                        } else {
                            // Not
                            isCoinciding = true
                            newMeetings.clear()
                            //  Toast.makeText(this, "Slot Not Available", Toast.LENGTH_SHORT).show()

                        }
                    } else if (endTimecalendarObject.get(Calendar.HOUR_OF_DAY) < endArray[0].toInt()) {
                        isCoinciding = true
                        newMeetings.clear()
                        // Toast.makeText(this, "Slot Not Available", Toast.LENGTH_SHORT).show()
                    }
                }
            } else if (startTimecalendarObject.get(Calendar.HOUR_OF_DAY) < startArray[0].toInt()) {
                if (endTimecalendarObject.get(Calendar.HOUR_OF_DAY) < startArray[0].toInt()) {
                    if (!isCoinciding) {
                        model.description = "NEW"
                        model.startTime =
                            startTimecalendarObject.get(Calendar.HOUR_OF_DAY).toString() + ":" + startTimecalendarObject.get(
                                Calendar.MINUTE
                            )
                        model.endTime =
                            endTimecalendarObject.get(Calendar.HOUR_OF_DAY).toString() + ":" + endTimecalendarObject.get(
                                Calendar.MINUTE
                            )
                        // newMeetings.add(model)
                        //  Toast.makeText(this, "Slot Available", Toast.LENGTH_SHORT).show()
                        break
                    }

                } else if (endTimecalendarObject.get(Calendar.HOUR_OF_DAY) == startArray[0].toInt()) {
                    if (endTimecalendarObject.get(Calendar.MINUTE) < startArray[1].toInt()) {
                        if (!isCoinciding) {
                            model.description = "NEW"
                            model.startTime =
                                startTimecalendarObject.get(Calendar.HOUR_OF_DAY).toString() + ":" + startTimecalendarObject.get(
                                    Calendar.MINUTE
                                )
                            model.endTime =
                                endTimecalendarObject.get(Calendar.HOUR_OF_DAY).toString() + ":" + endTimecalendarObject.get(
                                    Calendar.MINUTE
                                )
                            //   newMeetings.add(model)
                            //   Toast.makeText(this, "Slot Available", Toast.LENGTH_SHORT).show()
                            break
                        }
                    } else {
                        //  Toast.makeText(this, "Slot Not Available", Toast.LENGTH_SHORT).show()
                        newMeetings.clear()
                        isCoinciding = true
                    }
                } else if (endTimecalendarObject.get(Calendar.HOUR_OF_DAY) > endArray[0].toInt()) {
                    // Toast.makeText(this, "Slot Not Available", Toast.LENGTH_SHORT).show()
                    newMeetings.clear()
                    isCoinciding = true
                } else {
                    // Toast.makeText(this, "Slot Not Available", Toast.LENGTH_SHORT).show()
                    newMeetings.clear()
                    isCoinciding = true
                }


            } else if ((startArray[0].toInt()) < startTimecalendarObject.get(Calendar.HOUR_OF_DAY) && endArray[0].toInt() < startTimecalendarObject.get(
                    Calendar.HOUR_OF_DAY
                )
            ) {
                if (!isCoinciding) {

                    model.description = "NEW"
                    model.startTime =
                        startTimecalendarObject.get(Calendar.HOUR_OF_DAY).toString() + ":" + startTimecalendarObject.get(
                            Calendar.MINUTE
                        ).toString()
                    model.endTime =
                        endTimecalendarObject.get(Calendar.HOUR_OF_DAY).toString() + ":" + endTimecalendarObject.get(
                            Calendar.MINUTE
                        )

                    //  newMeetings.add(model)
                    /*Toast.makeText(
                        this, "Slot Available", Toast.LENGTH_SHORT
                    ).show()
*/
                } else {
                    newMeetings.clear()
                    /*  Toast.makeText(
                          this, "Slot Not Available", Toast.LENGTH_SHORT
                      ).show()*/
                }
            } else {
                newMeetings.clear()
                // Toast.makeText(this, "Slot Not Available", Toast.LENGTH_SHORT).show()
                isCoinciding = true
            }

        }
        if (!isCoinciding) {
            Toast.makeText(
                this, "Slot Available", Toast.LENGTH_SHORT
            ).show()
            newMeetings.add(model)
            convertedResponse!!.addAll(newMeetings)
            broadcast(convertedResponse!!)
        } else
            Toast.makeText(
                this, "Slot Not Available", Toast.LENGTH_SHORT
            ).show()
    }

    private fun broadcast(convertedResponse: ArrayList<ResponseModel>) {
        if (convertedResponse.size > 0) {
            var intent = Intent("response")
            var bundle = Bundle()
            bundle.putSerializable("convertedResponse", convertedResponse as Serializable)
            intent.putExtras(bundle)
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent)

        }

    }


    private fun openCalendar() {
        DatePickerDialog(
            this@MainActivity, dateSetListener,
            calendarObject.get(Calendar.YEAR),
            calendarObject.get(Calendar.MONTH),
            calendarObject.get(Calendar.DAY_OF_MONTH)
        ).show()

    }

    val dateSetListener =
        DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            calendarObject.set(Calendar.YEAR, year)
            calendarObject.set(Calendar.MONTH, monthOfYear)
            calendarObject.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            toolbar_title.text = dateFormat.format(calendarObject.time)
            callApiForDate(toolbar_title.text.toString().replace("-", "/"), false)


        }

    fun callApiForDate(date: String, isFirst: Boolean) {
        val stringRequest = StringRequest(
            Request.Method.GET, baseUrl + date,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    Log.d("response", response)
                    parseResponse(response, isFirst)
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
                }
            })


        mRequestQueue.add(stringRequest)
    }

    fun parseResponse(response: String, isFirst: Boolean) {
        try {
            val collectionType = object : TypeToken<List<ResponseModel>>() {}.type
            convertedResponse = Gson()
                .fromJson(response, collectionType) as ArrayList<ResponseModel>
            broadcast(convertedResponse!!)

            if (!isFirst) {
                if (presentCalendarObject.time <= calendarObject.time) {
                    tvScheduleMeeting.isEnabled = true
                } else
                    tvScheduleMeeting.isEnabled = false
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }

    private fun getPreviousDate(): String {
        calendarObject.time = (dateFormat.parse(toolbar_title.text.toString()))
        calendarObject.add(Calendar.DATE, -1)
        prevLayout.isEnabled = true
        callApiForDate(dateFormat.format(calendarObject.time).replace("-", "/"), false)
        return dateFormat.format(calendarObject.time)
    }

    private fun getNextDate(): String {
        prevLayout.isEnabled = true
        calendarObject.time = (dateFormat.parse(toolbar_title.text.toString()))
        calendarObject.add(Calendar.DATE, 1)
        return dateFormat.format(calendarObject.time)

    }

    private fun replaceFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.main_fragment_frame, HomeFragment())
            .commitAllowingStateLoss()
    }

    private fun getCurrentDate(): String {
        calendarObject = Calendar.getInstance()
        presentCalendarObject = Calendar.getInstance()
        calendarDate = calendarObject.time
        val formattedDate = dateFormat.format(calendarDate)
        return formattedDate
    }

}
