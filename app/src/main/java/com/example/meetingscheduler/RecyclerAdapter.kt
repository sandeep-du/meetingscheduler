package com.example.meetingscheduler

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class RecyclerAdapter(
    var responseList: List<ResponseModel>?
) : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.recycleradapterlayout, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(responseList!![position])
    }

    override fun getItemCount(): Int {
        return responseList!!.size
    }

    fun updateList(responseModel: List<ResponseModel>) {
        responseList = responseModel
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(responseList: ResponseModel) {
            val textViewTiming = itemView.findViewById(R.id.timing) as TextView
            val textViewDescription = itemView.findViewById(R.id.description) as TextView
            textViewDescription.text = responseList.description
            textViewTiming.text = responseList.startTime + " - " + responseList.endTime
        }
    }
}