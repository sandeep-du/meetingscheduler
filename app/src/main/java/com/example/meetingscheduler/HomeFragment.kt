package com.example.meetingscheduler

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.homefragmentlayout.view.*
import java.util.*

@SuppressLint("ValidFragment")
class HomeFragment() : Fragment() {
    var recyclerAdapter: RecyclerAdapter? = null
    internal var responseList: List<ResponseModel> = ArrayList()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.homefragmentlayout, container, false)
        recyclerAdapter = RecyclerAdapter(responseList)
        view.meetingRecyclerView.apply {
            layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
            adapter = recyclerAdapter
        }

        LocalBroadcastManager.getInstance(activity!!).registerReceiver(
            responseReceiver,
            IntentFilter("response")
        )
        return view
    }

    private val responseReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            responseList = intent.extras["convertedResponse"] as List<ResponseModel>
            recyclerAdapter!!.updateList(responseList)
        }
    }
}